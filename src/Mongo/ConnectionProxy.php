<?php


namespace Terra\Mongo;


use Terra\Mongo\Strategy;
use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Database;

class ConnectionProxy
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var array ['operation' => Database]
     */
    private $pool;

    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->pool = [];
    }

    /**
     * @param $name
     * @param $operation
     * @return Collection
     */
    public function selectCollection($name, $operation = Strategy::READ)
    {
        if (!array_key_exists($operation, $this->config)) {
            throw new \InvalidArgumentException(sprintf(
                'Unknown operation: "%s". It can be only read or write.',
                $operation
            ));
        }

        $connection = $this->getConnection($operation);

        return $connection->selectCollection($name);
    }

    /**
     * @param $name
     * @return \MongoDB\DeleteResult
     */
    public function cleanCollection($name)
    {
        return $this->selectCollection($name, Strategy::WRITE)->deleteMany([]);
    }

    /**
     * @param string $operation
     * @return Database
     */
    private function getConnection($operation)
    {
        if (!array_key_exists($operation, $this->pool)) {
            $clientConfig = $this->config[$operation];
            $client = new Client(
                $clientConfig['uri'],
                $clientConfig['uriOptions'],
                $clientConfig['driverOptions']
            );

            $this->pool[$operation] = $client->selectDatabase($clientConfig['database']);
        }

        return $this->pool[$operation];
    }
}
