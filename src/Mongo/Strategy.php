<?php


namespace Terra\Mongo;


final class Strategy
{
    const READ = 'read';

    const WRITE = 'write';
}
