<?php


namespace Terra\Authentication;


interface AuthenticatorInterface
{
    /**
     * @param string $token
     * @return boolean
     */
    public function authenticate($token);
}
