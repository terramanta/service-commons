<?php


namespace Terra\Authentication;


class NullAuthenticator implements AuthenticatorInterface
{
    /**
     * @var bool
     */
    private $value;

    /**
     * NullAuthenticator constructor.
     * @param bool $value
     */
    public function __construct($value = true)
    {
        $this->value = $value;
    }

    /**
     * @param string $token
     * @return boolean
     */
    public function authenticate($token)
    {
        return $this->value;
    }
}
