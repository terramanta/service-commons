<?php


namespace Terra\Authentication;

use GuzzleHttp\Client;

class Authenticator implements AuthenticatorInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Authenticator constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->client = new Client($config);
    }

    /**
     * @param string $token
     * @return boolean
     */
    public function authenticate($token)
    {
        if (!$token) {
            return false;
        }

        try {
            return $this->client->get('/auth/'.$token)->getStatusCode() === 200;
        } catch (\Exception $e) {
            return false;
        }
    }
}
