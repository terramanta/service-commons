<?php


namespace Terra\MiddleWare;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Terra\Authentication\AuthenticatorInterface;

class Authentication
{
    /**
     * @var AuthenticatorInterface
     */
    private $authenticator;

    public function __construct(AuthenticatorInterface $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        if ($request->getUri()->getPath() === '/swagger') {
            return $next($request, $response);
        }

        $queryString = $request->getUri()->getQuery();

        if ($this->authenticator->authenticate($this->parseToken($queryString))) {
            return $next($request, $response);
        }

        return $this->notAuthorized($response);
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    private function notAuthorized($response)
    {
        $body = $response->getBody();
        $body->rewind();
        $body->write(json_encode(['code' => 401, 'message' => 'Authentication token is invalid.']));

        return $response
            ->withStatus(401)
            ->withHeader('Content-Type', 'application/json')
            ->withBody($body);
    }

    private function parseToken($queryString)
    {
        $params = \GuzzleHttp\Psr7\parse_query($queryString, true);

        return array_key_exists('token', $params)
            ? $params['token']
            : null;
    }
}
