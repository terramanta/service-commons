<?php


namespace Terra\MiddleWare;

class Cors
{
    /**
     * @var string
     */
    private $origin;
    /**
     * @var array
     */
    private $methods;

    public function __construct($origin = '*', array $methods = ['GET'])
    {
        $this->origin = $origin;
        $this->methods = $methods;
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        return $next($request, $response)
            ->withHeader('Access-Control-Allow-Origin', $this->origin)
            ->withHeader('Access-Control-Allow-Methods', implode(', ', $this->methods));
    }
}
