<?php


namespace Terra\Test\Authenticator;

use Terra\Authentication\NullAuthenticator;

class NullAuthenticatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param $expected
     * @dataProvider values
     */
    public function testAuthenticate($expected)
    {
        $authenticator = new NullAuthenticator($expected);
        $this->assertEquals($expected, $authenticator->authenticate('test'));
    }

    public function values()
    {
        return [
            [true],
            [false]
        ];
    }
}
