<?php


namespace Terra\Test\Authenticator;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Terra\Authentication\Authenticator;

class AuthenticatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param $token
     * @test
     * @dataProvider falseTokens
     */
    public function shouldReturnFalseWithoutToken($token)
    {
        $authenticator = new Authenticator([]);
        $this->assertFalse($authenticator->authenticate($token));
    }

    public function falseTokens()
    {
        return [
            [false],
            [''],
            [null]
        ];
    }

    /**
     * @test
     * @dataProvider exceptions
     * @param $exception
     */
    public function shouldReturnFalseIfClientThrowsException($exception)
    {
        $mockHandler = new MockHandler([$exception]);

        $authenticator = new Authenticator(['handler' => HandlerStack::create($mockHandler)]);
        $this->assertFalse($authenticator->authenticate('token'));
    }

    public function exceptions()
    {
        return [
            [new RequestException('Communication error', new Request('GET', '/auth/token'))],
        ];
    }

    /**
     * @param $response
     * @param $expected
     * @dataProvider responses
     */
    public function testAuthenticate($response, $expected)
    {
        $mockHandler = new MockHandler([$response]);
        $authenticator = new Authenticator(['handler' => HandlerStack::create($mockHandler)]);

        $this->assertEquals($expected, $authenticator->authenticate('token'));
    }

    public function responses()
    {
        return [
            [new Response(200), true],
            [new Response(404), false],
            [new Response(500), false],
            [new Response(401), false],
            [new Response(403), false]
        ];
    }
}
