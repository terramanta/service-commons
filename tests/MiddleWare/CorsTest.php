<?php


namespace Terra\Test\MiddleWare;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Terra\MiddleWare\Cors;

class CorsTest extends \PHPUnit_Framework_TestCase
{
    public function testCors()
    {
        $cors = new Cors('*.test.ltd', ['GET', 'POST']);
        $response = $cors(new ServerRequest('GET', '/'), new Response(), function($response) {
            return $response;
        });

        $this->assertEquals('*.test.ltd', $response->getHeaderLine('Access-Control-Allow-Origin'));
        $this->assertEquals('GET, POST', $response->getHeaderLine('Access-Control-Allow-Methods'));
    }
}
