<?php


namespace Terra\Test\MiddleWare;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Terra\Authentication\NullAuthenticator;
use Terra\MiddleWare\Authentication;

/**
 * Class AuthenticationTest
 * @package Terra\Test\MiddleWare
 * In those tests I'm using request-response psr7 implementation from guzzle to avoid tons of mocking
 */
class AuthenticationTest extends \PHPUnit_Framework_TestCase
{
    public function testSwagger()
    {
        $request = new ServerRequest('GET', '/swagger');
        $response = new Response();
        $called = false;
        $next = function($request, Response $response) use (&$called) {
            $called = true;
            return $response;
        };

        $middleware = new Authentication(new NullAuthenticator(true));
        $newResponse = $middleware($request, $response, $next);

        $this->assertTrue($called);
        $this->assertEquals($response, $newResponse);
    }

    public function test401()
    {
        $request = new ServerRequest('GET', '/super-query?token=token');
        $middleware = new Authentication(new NullAuthenticator(false));
        $response = $middleware($request, new Response(), function($response) {
            return $response;
        });

        $this->assertEquals(401, $response->getStatusCode());
        $this->assertEquals('application/json', $response->getHeaderLine('Content-Type'));
    }
}
